#ifndef DRAWING_SORTABLE_H_INCLUDED
#define DRAWING_SORTABLE_H_INCLUDED

class DrawingSortable {
	private:
		static int comparisons_;
		static int swaps_;
		static int R_norm_;
		static int G_norm_;
		static int B_norm_;
		static int R_assign_;
		static int G_assign_;
		static int B_assign_;
		static int R_comp_;
		static int G_comp_;
		static int B_comp_;
		int value_;
		int x_coord_;
		int y_coord_;
		int unit = 20;
		int sleeptime = 80000;
		
		void draw(int r, int g, int b) const;
		
	public:
		
		
		DrawingSortable();
		
		DrawingSortable& operator=(DrawingSortable const &other);
		
		bool operator<(DrawingSortable const &other);
		
		bool operator<=(DrawingSortable const &other);
		
		int get_value();
		
		void set_value(int v);
		
		void set_x_coord(int x);
		
		void set_y_coord(int y);
		
		static int get_comparisions();
		
		static void set_comparisons(int n);
		
		static int get_swaps();
		
		static void set_swaps(int n);
		
};
#endif
