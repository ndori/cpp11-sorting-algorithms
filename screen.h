#ifndef SCREEN_H_INCLUDED
#define SCREEN_H_INCLUDED

class Screen {
	public:
		static SDL_Surface *screen;
		static const int R_screen = 191;
		static const int G_screen = 189;
		static const int B_screen = 193;
		static const int SCREEN_WIDTH = 620;
		static const int SCREEN_HEIGHT = 450;
		static const int X_COORD_START = 10;
		static const int Y_COORD_START = SCREEN_HEIGHT - 10;
		
		static void clear();
			
};


#endif
