main: drawing_sortable.o main.o screen.o
		g++ -g -o main drawing_sortable.o main.o screen.o -lSDL -lSDL_gfx
		
main.o: main.cpp drawing_sortable.h sorting_algorithms.h screen.h
		g++ -std=c++11 -Wall -Wdeprecated -pedantic -g -c main.cpp -o main.o

drawing_sortable.o: drawing_sortable.cpp drawing_sortable.h screen.h
		g++ -std=c++11 -Wall -Wdeprecated -pedantic -g -c drawing_sortable.cpp -o drawing_sortable.o

screen.o: screen.h
		g++ -std=c++11 -Wall -Wdeprecated -pedantic -g -c screen.cpp -o screen.o

