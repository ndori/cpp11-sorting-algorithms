#ifndef SORTING_ALGORITHMS_H_INCLUDED
#define SORTING_ALGORITHMS_H_INCLUDED

template <typename T>
void bubble_sort(T *arr, int n) {
	int i, j;
	for (i = n-1; i > 0; i--) {
		for (j = 0; j < i; j++) {
			if (arr[j+1] < arr[j]) {
				T temp = arr[j];
				arr[j] = arr[j+1]; //ertekadas -> rajzol
				arr[j+1] = temp; //ertekadas -> rajzol
			}
		}
	}
}


template <typename T>
void selection_sort(T *arr, int n) {
	int i, j;
	for (i = 0; i < n - 1; i++) {
		int minindex = i;
		for (j = i + 1; j < n; j++) {
			if (arr[j] < arr[minindex]) {
				minindex = j;
			}
		}
		
		if (minindex != i) {
			T temp = arr[minindex];
			arr[minindex] = arr[i];
			arr[i] = temp;
		}
	}
}


template <typename T>
void gnome_sort(T *arr, int n) {
	int i = 0;
	while (i < n) { 
		if (i == 0 || arr[i-1] <= arr[i]) {
		i++;
		}
		else {
			T tmp = arr[i];
			arr[i] = arr[i-1];
			arr[i-1] = tmp;
			i--;
		}
	}
}

#endif
