#include <SDL/SDL.h> //Linux miatt igy kell
#include <SDL/SDL_gfxPrimitives.h>
//#include <SDL/SDL_ttf.h>
#include <iostream>
#include <unistd.h>  //sleep, usleep
#include <cstdlib>	 //std::rand, std::srand
#include <algorithm> //std::swap
#include <string>

#include "sorting_algorithms.h"
#include "drawing_sortable.h"
#include "screen.h"


int main(int argc, char* args[]) {
	std::srand(0);
	const int ARRAY_SIZE = 20;
	
	//STD initalization
	SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER);
	Screen::screen = SDL_SetVideoMode(Screen::SCREEN_WIDTH, Screen::SCREEN_HEIGHT, 0, SDL_ANYFORMAT);
	if (!Screen::screen) {
		fprintf(stderr, "Cannot open window!\n");
		exit(1);
	}
	SDL_WM_SetCaption("Sorting algorithm", "Sorting algorithm");
	
	//generating unique values from 1 to 20 in random order
	int values[ARRAY_SIZE];
	for (int i = 0; i < ARRAY_SIZE; ++i) {
		values[i] = ARRAY_SIZE - i;
	}
	for (int i = 0; i < ARRAY_SIZE/2; ++i) {
		int index = std::rand() % ARRAY_SIZE;
		std::swap(values[i], values[index]);
	}
	
	Screen::clear();

	DrawingSortable t1[ARRAY_SIZE];
	for (int i = 0; i < ARRAY_SIZE; ++i) {
		t1[i].set_x_coord(Screen::X_COORD_START + i *30);
		t1[i].set_y_coord(Screen::Y_COORD_START);
		t1[i].set_value(values[i]); //kirajzolja magat
	}
	
	stringRGBA(Screen::screen, 10, 10, "Selection sort" , 0, 0, 0, 255);	
	stringRGBA(Screen::screen, Screen::SCREEN_WIDTH / 3, 10, "Comparisions: ", 0, 0, 0, 255);
	stringRGBA(Screen::screen, (Screen::SCREEN_WIDTH / 3) *2 , 10, "Assignments: ", 0, 0, 0, 255);											
	SDL_Flip(Screen::screen); // eredeti allapot a kepernyore
	sleep(1);
	
	selection_sort<DrawingSortable>(t1, ARRAY_SIZE);
	sleep(1);
	
	Screen::clear();
	DrawingSortable::set_comparisons(0);
	DrawingSortable::set_swaps(0);
	
	for (int i = 0; i < ARRAY_SIZE; ++i) {
		t1[i].set_x_coord(Screen::X_COORD_START + i *30);
		t1[i].set_y_coord(Screen::Y_COORD_START);
		t1[i].set_value(values[i]);
	}
	
	stringRGBA(Screen::screen, 10, 10, "Bubble sort", 0, 0, 0, 255);
	stringRGBA(Screen::screen, Screen::SCREEN_WIDTH / 3, 10, "Comparisions: ", 0, 0, 0, 255);
	stringRGBA(Screen::screen, (Screen::SCREEN_WIDTH / 3) *2 , 10, "Assignments: ", 0, 0, 0, 255);	
	SDL_Flip(Screen::screen); // eredeti allapot a kepernyore
	sleep(1);
	
	bubble_sort<DrawingSortable>(t1, ARRAY_SIZE);
	sleep(1);
	
	Screen::clear();
	DrawingSortable::set_comparisons(0);
	DrawingSortable::set_swaps(0);

	for (int i = 0; i < ARRAY_SIZE; ++i) {
		t1[i].set_x_coord(Screen::X_COORD_START + i *30);
		t1[i].set_y_coord(Screen::Y_COORD_START);
		t1[i].set_value(values[i]);
	}
	
	stringRGBA(Screen::screen, 10, 10, "Gnome sort", 0, 0, 0, 255);	
	stringRGBA(Screen::screen, Screen::SCREEN_WIDTH / 3, 10, "Comparisions: ", 0, 0, 0, 255);
	stringRGBA(Screen::screen, (Screen::SCREEN_WIDTH / 3) *2 , 10, "Assignments: ", 0, 0, 0, 255);	
	SDL_Flip(Screen::screen); // eredeti allapot a kepernyore
	sleep(1);
	
	gnome_sort<DrawingSortable>(t1, ARRAY_SIZE);
 
    //closing the window
	SDL_Quit();
 
	return 0;
}
