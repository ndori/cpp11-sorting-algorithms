#include <SDL/SDL.h>
#include <SDL/SDL_gfxPrimitives.h>
#include <unistd.h> 
#include <string>

#include "drawing_sortable.h"
#include "screen.h"


int DrawingSortable::comparisons_ = 0;
int DrawingSortable::swaps_ = 0;
int DrawingSortable::R_norm_ = 80;
int DrawingSortable::G_norm_ = 76;
int DrawingSortable::B_norm_ = 86;
int DrawingSortable::R_assign_ = 183;
int DrawingSortable::G_assign_ = 217;
int DrawingSortable::B_assign_ = 81;
int DrawingSortable::R_comp_ = 255;
int DrawingSortable::G_comp_ = 109;
int DrawingSortable::B_comp_ = 145;


DrawingSortable::DrawingSortable() {}

void DrawingSortable::draw(int R, int G, int B) const {
	boxRGBA(Screen::screen, x_coord_, Screen::Y_COORD_START - value_ * unit , x_coord_ + 30, Screen::Y_COORD_START, R, G, B, 255);
	rectangleRGBA(Screen::screen, x_coord_, Screen::Y_COORD_START - value_ * unit , x_coord_ + 30, Screen::Y_COORD_START, Screen::R_screen, Screen::G_screen, Screen::B_screen, 255);

}

DrawingSortable& DrawingSortable::operator=(DrawingSortable const &other) {
	draw(Screen::R_screen, Screen::G_screen, Screen::B_screen); // letorli az elozo magat
	value_ = other.value_; // csak ez valtozik mert marad a helyen
	++swaps_;
	boxRGBA(Screen::screen, 520, 10 , 570, 20, Screen::R_screen, Screen::G_screen, Screen::B_screen, 255);
	SDL_Flip(Screen::screen);
	stringRGBA(Screen::screen, 520, 10, std::to_string(swaps_).c_str(), 0, 0, 0, 255);
	draw(R_assign_, G_assign_, B_assign_); //kizoldul
	SDL_Flip(Screen::screen);
	usleep(sleeptime);
	draw(R_norm_, G_norm_, B_norm_); //normi szinu lesz ujra
	SDL_Flip(Screen::screen);
	usleep(sleeptime);
	return *this;
}

bool DrawingSortable::operator<(DrawingSortable const &other) {
	++comparisons_;
	boxRGBA(Screen::screen, 320, 10 , 370, 20, Screen::R_screen, Screen::G_screen, Screen::B_screen, 255);
	SDL_Flip(Screen::screen);
	stringRGBA(Screen::screen, 320, 10, std::to_string(comparisons_).c_str(), 0, 0, 0, 255);
	draw(R_comp_, G_comp_, B_comp_); //piros lesz
	other.draw(R_comp_, G_comp_, B_comp_); //o is piros lesz
	SDL_Flip(Screen::screen);
	usleep(sleeptime);
	draw(R_norm_, G_norm_, B_norm_);
	other.draw(R_norm_, G_norm_, B_norm_);
	SDL_Flip(Screen::screen);
	usleep(sleeptime);
	return value_ < other.value_;
}

bool DrawingSortable::operator<=(DrawingSortable const &other) {
	++comparisons_;
	boxRGBA(Screen::screen, 320, 10 , 370, 20, Screen::R_screen, Screen::G_screen, Screen::B_screen, 255);
	SDL_Flip(Screen::screen);
	stringRGBA(Screen::screen, 320, 10, std::to_string(comparisons_).c_str(), 0, 0, 0, 255);
	draw(R_comp_, G_comp_, B_comp_);
	other.draw(R_comp_, G_comp_, B_comp_); 
	SDL_Flip(Screen::screen);
	usleep(sleeptime);
	draw(R_norm_, G_norm_, B_norm_); 
	other.draw(R_norm_, G_norm_, B_norm_);
	SDL_Flip(Screen::screen);
	usleep(sleeptime);
	return value_ <= other.value_;
}

int DrawingSortable::get_comparisions() {
	return comparisons_;
}

int DrawingSortable::get_swaps() {
	return swaps_;
}

int DrawingSortable::get_value() {
	return value_;
}

void DrawingSortable::set_value(int v) {
	value_ = v;
	draw(R_norm_, G_norm_, B_norm_); // kirajzolja magat
}

void DrawingSortable::set_x_coord(int x) {
	x_coord_ = x;
}

void DrawingSortable::set_y_coord(int y) {
	y_coord_ = y;
}

void DrawingSortable::set_comparisons(int n) {
	comparisons_ = n;
}

void DrawingSortable::set_swaps(int n) {
	swaps_ = n;
}


